FROM golang:latest

ADD ./* /root/
WORKDIR /root/
RUN go build

FROM centos:latest
WORKDIR /root/
RUN yum install -y wget gnupg createrepo rpm-sign expect
RUN echo "%_signature gpg" > /root/.rpmmacros
RUN echo "%_gpg_name gozilla" >> /root/.rpmmacros
COPY --from=0 /root/gozilla-rpm /usr/bin/
COPY --from=0 /root/buildrepo.sh /usr/bin/
COPY --from=0 /root/rpm-sign.exp /usr/bin/
COPY --from=0 /root/gozilla.yml /root/
