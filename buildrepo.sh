#!/bin/bash

set -e

# $1 = repo
# $2 = distrib
# $3 = dir
# $4 = gpg key id
# $5 = gpg passphrase

echo "Generate repo for $1/$2 in dir $3"

cd $3
createrepo .

#!/bin/bash

export PASS=$5

resign (){
  echo "signing $1"
  rpm-sign.exp $1
}
export -f resign
find . -type f -name "*.rpm" | xargs -I {} bash -c 'resign "{}"'
